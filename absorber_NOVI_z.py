# Aims to reproduce Figure 1 from Lorrie's original draft
import numpy as np
import h5py
import sys

# Read absorber catalogue data
path = '/home/mitchell/MUSE/Catalogs/'
absorber_file = path+'matched-OVI-cat-Lorrie_Sowgat_final_measurements.dat'

names =   ['QSO',          'z_abs', 'logN', 'logN_err', 'final_flag']
formats = [('string', 14), 'float', 'float', 'float', 'int']
dtype = np.dtype({'names':names, 'formats':formats})

cat = np.loadtxt(absorber_file,skiprows=1, dtype=dtype,usecols=(0,1,2,3,11), unpack=True)

z_abs = cat[1]
logN = cat[2]
logN_err = cat[3]
final_flag = cat[4]

ok = final_flag >= 1
print "Total number of absorbers with final flag = 1,2,3 is", len(final_flag[ok])

#### Plotting #######
sys.path.append("Utilities")
from utilities_plotting import *

nrow = 1; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow*1.2],
                    'legend.fontsize':9,
                    'font.size':11,
                    'axes.labelsize':11})

# definitions for the axes
left, width = 0.17, 0.65*0.93
bottom, height = 0.16, 0.65*0.95
spacing = 0.0125

rect_scatter = [left, bottom, width, height]
rect_histx = [left, bottom + height + spacing, width, 0.2*0.95]
rect_histy = [left + width + spacing, bottom, 0.2*0.95, height]

# start with a rectangular Figure

py.figure()

ax_scatter = py.axes(rect_scatter)
ax_scatter.tick_params(direction='in', top=True, right=True)
ax_histx = py.axes(rect_histx)
ax_histx.tick_params(direction='in', labelbottom=False)
ax_histy = py.axes(rect_histy)
ax_histy.tick_params(direction='in', labelleft=False)

ax_scatter.errorbar(z_abs[ok], logN[ok], yerr=logN_err[ok], fmt="or", mec="k", mew=0.5)

ax_scatter.set_xlim((0.1, 0.75))
ax_scatter.set_xticks([0.1,0.2,0.3,0.4,0.5,0.6,0.7])
ax_scatter.set_ylim((12.8, 14.9))

ax_scatter.set_xlabel(r"$z_{\mathrm{abs}}$")
ax_scatter.set_ylabel(r"$\log_{10}(N_{\mathrm{OVI}} \, /\mathrm{cm^{-2}})$")

bins_z = np.arange(0.1, 0.8, 0.05)
ax_histx.hist(z_abs[ok],bins=bins_z)
ax_histx.set_xlim(ax_scatter.get_xlim())
ax_histx.set_ylabel(r"$N$")
ax_histx.set_yticks([0,10,20])

bins_logN = np.arange(12.75, 15.25, 0.25)
ax_histy.hist(logN[ok], orientation="horizontal",bins=bins_logN)
ax_histy.set_ylim(ax_scatter.get_ylim())
ax_histy.set_xlabel(r"$N$")
ax_histy.set_xticks([0,10,20,30,40])

figname = "fig1_logNOVI_zabs.pdf"
py.savefig("../Figures/"+figname)
py.show()
