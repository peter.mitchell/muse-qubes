import os
import glob
import numpy as np
from scipy.integrate import simps
from scipy.special import wofz
from scipy.signal import convolve
from scipy.optimize import minimize
from scipy.interpolate import interp1d
# Get a feature by ion name + wavelength. Informatino returned including
# oscillator strength and damping coefficient. Taken from atom.dat
# which is adapted from the atom.dat file distributed with vpfit

# Set some physical constants in CGS.
e = 4.8032e-10 # electron charge in stat-coulumb
m_e = 9.10938e-28 # electron mass
c = 2.9979e10 # cm/s
c_As = 2.9979e18
c_kms = 2.9979e5
k = 1.38065e-16 # erg/K


def getfeature(name='HI', wave=1215.6701):

    # Read in the features
    filename = 'atom.npy'
    features = np.load(filename)
   
    # Get features with this ion                          
    index = np.where(features['name'] == name)
    features = features[index]
   
    # Get the closest match in wavelength
    index = np.argmin(np.absolute(wave - features['wave']))
    feature = features[index]
    
    return feature
   

#def voigt(x, alpha, gamma):
#   
#   sigma = alpha/np.sqrt(2*np.log(2))
#   
#   return np.real(wofz((x + 1j*gamma)/sigma/np.sqrt(2)))/sigma/np.sqrt(2*np.pi)
   

def kernel_gaussian(wave, wave_mean, sigma):
   
    kernel = 1/np.sqrt(2*sigma**2*np.pi)*np.exp(-(wave - wave_mean)**2/(2*sigma**2))
    kernel = kernel/np.sum(kernel)
   
    return kernel

def voigt(a, u):
   
    return wofz(u + 1j * a).real

def calctau(wave, logN, b, redshift=0.0, name='HI', wave_feature=1215.67):

    feature = getfeature(name, wave_feature)

    # Go from logN to N
    N = 10.0**logN
   
    # calculate the rest-frame frequency in 1/s
    nu0 = c_As/feature['wave']
   
    # Go into ion rest-frame
    wave_rest = wave/(1 + redshift)
   
    # Calculate nu in the rest-frame
    nu_rest = c_As/wave_rest

    dopplerWidth = b*nu0/c_kms
    a = feature['Gamma']/4.0/np.pi/dopplerWidth
    u = (nu_rest - nu0)/dopplerWidth
   
    H = voigt(a, u)
   
    # calculate tau
    tau = N*np.pi*e**2/m_e/c*feature['f']/np.sqrt(np.pi)/dopplerWidth*H
   
    return tau


# Wavelength observed in Angstroms
# Continuum normalized flux and error
def measureWr(wave_obs, flux, error, z, dv, name='HI', wave_feature=1215.67):
   
    # Go into the rest-frame
    wave_rest = wave_obs/(1 + z)
   
    # Get the feature
    feature = getfeature(name, wave_feature)
   
    # Go from dv to dW
    dW = dv/c_kms*feature['wave']

   
    # Get the pixels within +/- dv/2
    index = np.where((wave_rest >= feature['wave']-dW/2.0) \
                     & (wave_rest <= feature['wave']+dW/2.0))[0]
   
    # Return NAN is the spectrum doesn't cover the needed range
    if (index.size == 0) | (np.min(wave_rest) > feature['wave']-dW/2.0) | (np.max(wave_rest) < feature['wave']+dW/2.0):
        Wr = np.nan
        WrErr = np.nan
      
    else:
      
        Wr = np.sum((1.0 - flux[index])*(wave_rest[index+1] - wave_rest[index]))
        WrErr = np.sqrt(np.sum((error[index]*(wave_rest[index+1] - wave_rest[index]))**2))
      
    return Wr, WrErr

def calcflux_pcov(wave, logN, b, redshift=0.0, pcov=1.0, name='HI', wave_feature=1215.67):
   
    tau = calctau(wave, logN, b, redshift, name, wave_feature)
   
    return 1 - pcov + pcov*np.exp(-tau)
   

def calcflux(wave, logN, b, redshift=0.0, name='HI', wave_feature=1215.67):
   
    tau = calctau(wave, logN, b, redshift, name, wave_feature)
   
    return np.exp(-tau)
   
def calconeminusflux(wave, logN, b, redshift=0.0, name='HI', wave_feature=1215.67):
   
    flux = calcflux(wave, logN, b, redshift, name, wave_feature)
   
    return 1 - flux

def mockspectrum(wave, logNarray, bArray, redshiftArray, nameArray, wave_feature_array,
                 resvel=0.0, rescos=0):

  
    # If COS get native pixel scale in preparation for convolution with lsf 
    if rescos != 0:
        wave_in = np.copy(wave)
        wave_mean = wave_mean = np.mean(wave)
     
        # Determine whether it is G130M or G160M
        if wave_mean < 1425:
            pixscale = 9.97e-3
        else:
            pixscale = 12.23e-3
        
        wave = np.arange(np.min(wave_in) - 5, np.max(wave_in) + 5, pixscale)
    
    tau = np.zeros(len(wave))
    for i in range(len(nameArray)):
     
        tau = tau + calctau(wave, logNarray[i], bArray[i], redshiftArray[i],
                         nameArray[i], wave_feature_array[i])
    flux = np.exp(-tau)
    if resvel > 0.0:
     
     
        # Assume that the wavelength interval is small
        wave_mean = np.mean(wave)
        dW_fwhm = resvel/c_kms*wave_mean
        dW_sigma = dW_fwhm/2/np.sqrt(2*np.log(2))
     
        pixScale = wave[len(wave)/2] - wave[len(wave)/2 - 1]  
        dPix_sigma = dW_sigma/pixScale
     
     
        pix_kernel = np.concatenate((-1*np.arange(1, 10*dPix_sigma, 1), [0],
                                     np.arange(1, 10*dPix_sigma, 1)))
        pix_kernel.sort()
     
        pix_mean = 0.0
  
        kernel = kernel_gaussian(pix_kernel, pix_mean, dPix_sigma)
        # Continuum subtract and invert to prevent edge effects
        flux = flux - 1
        flux = flux*-1
        flux = convolve(flux, kernel, 'same')
     
        # Now undo continuum subtraction and inversion
        flux = flux*-1
        flux = 1 + flux
   
    # Convolve with COS LSF if needed
    if rescos != 0:
     
        path = os.environ['PYOBS'] + '/absorption/COS/'
        names = ['COS_res1100.npy', 'COS_res1250.npy', 'COS_res1400.npy', 'COS_res1550.npy',
                 'COS_res1700.npy', 'COS_res1150.npy', 'COS_res1300.npy', 'COS_res1450.npy',
                 'COS_res1600.npy', 'COS_res1750.npy', 'COS_res1200.npy', 'COS_res1350.npy', 
                 'COS_res1500.npy', 'COS_res1650.npy']
        names_wave = [1100, 1250, 1400, 1550,
                      1700, 1150, 1300, 1450,
                      1600, 1750, 1200, 1350, 
                      1500, 1650]
        index = np.argmin(np.abs(names_wave - wave_mean))
        resfilename = path + names[index]
        #print resfilename
        kernel = np.load(resfilename) 
        kernel = kernel['kernel']
        kernel = kernel/np.sum(kernel)
     
        flux = flux - 1
        flux = flux*-1
     
        flux = convolve(flux, kernel, 'same')
        
        flux = flux*-1
        flux = 1 + flux
        
        flux_spline = interp1d(wave, flux)
        flux = flux_spline(wave_in)

    return flux
  
  
  
def calcdv90(redshift, logNarray, bArray, redshiftArray, nameArray, wave_feature_array,
             resvel=0.0, rescos=0):

    feature = getfeature(nameArray[0], wave_feature_array[0])
    wave0 = (1 + redshift)*feature['wave']
   
    wave = np.arange(wave0-30.0, wave0+30.0, 0.0001)
    velocity = (wave - wave0)/wave0*c_kms
    flux = mockspectrum(wave, logNarray, bArray, redshiftArray, nameArray,
                       wave_feature_array)
   
    tau = -np.log(flux)
      
    tau_cumulative = np.cumsum(tau)/np.sum(tau)
    tau_cumulative_interp = interp1d(tau_cumulative, velocity)
    return tau_cumulative_interp([0.05, 0.95])
   

def mockspectrum_foremcee(alpha, wave, nameArray, wave0Array, 
                          resvel=0.0, rescos=0):
   
    # Set up the component arrays
    logNarray = np.zeros(len(nameArray))
    bArray = np.zeros(len(nameArray))
    redshiftArray = np.zeros(len(nameArray))
    
    for i in range(len(nameArray)):
      
        logNarray[i] = alpha[3*i + 0]
        bArray[i] = alpha[3*i + 1]
        redshiftArray[i] = alpha[3*i + 2]
      
    #for i in range(len(nameArray)):
    #   print nameArray[i], wave0Array[i], redshiftArray[i], bArray[i], logNarray[i]

    model = mockspectrum(wave,
                         logNarray, bArray, redshiftArray, nameArray, wave0Array, 
                         resvel, rescos)
   

    return model


def logLikelihood_foremcee(alpha, wave, flux, error, nameArray, wave0Array, 
                          resvel=0.0, rescos=0):
   
    # Set up the component arrays
    logNarray = np.zeros(len(nameArray))
    bArray = np.zeros(len(nameArray))
    redshiftArray = np.zeros(len(nameArray))
    
    for i in range(len(nameArray)):
      
        logNarray[i] = alpha[3*i + 0]
        bArray[i] = alpha[3*i + 1]
        redshiftArray[i] = alpha[3*i + 2]
      
    #for i in range(len(nameArray)):
    #   print nameArray[i], wave0Array[i], redshiftArray[i], bArray[i], logNarray[i]

    model = mockspectrum(wave,
                         logNarray, bArray, redshiftArray, nameArray, wave0Array, 
                         resvel, rescos)
   
    logLikelihood = np.sum(np.log(1/np.sqrt(2*np.pi)/error) + -(flux - model)**2/2/error**2)

    return logLikelihood


def calcn_linear(W, name='HI', wave=1215.6701):
   
    # Get the feature oscillator strength
    feature = getfeature(name, wave)
   
    wave_cm = feature['wave']*1e-8
 
    # Calculate equivalent width in linear regime and convert from cm to A
    N = W/(wave_cm**2/c**2)/np.pi/e**2*m_e/feature['f']/1e8

    return N
   
def calcw_linear(N, name='HI', wave=1215.6701):
   
    # Get the feature oscillator strength
    feature = getfeature(name, wave)
   
    wave_cm = feature['wave']*1e-8
 
    # Calculate equivalent width in linear regime and convert from cm to A
    W = (wave_cm**2/c**2)*np.pi*e**2/m_e*feature['f']*N*1e8

    return W
   
   
   
def calcw(logN, b, name='HI', wave_feature=1215.6701, dWave=300):
   
    feature = getfeature(name, wave_feature)
    wave = np.arange(feature['wave']-dWave, feature['wave']+dWave, 0.01)
    oneminusflux = calconeminusflux(wave, logN, b, 0.0, name, wave_feature)
   
    W = simps(oneminusflux, wave)
    
    return W
   
def calcabs_w_minus_w(logN, b, W, name='HI', wave_feature=1215.6701, dWave=300):
   
    toReturn = np.abs(calcw(logN, b, name, wave_feature, dWave) - W)
   
    return toReturn
   
   
def calclogN(W, b, name='HI', wave_feature=1215.6701, dWave=1000):
   
    logNarray = np.arange(7.0, 24.0, 0.1)
    logWarray = np.zeros(len(logNarray))

    # Create a curve of growth
    for i in range(len(logNarray)):
        logWarray[i] = np.log10(calcw(logNarray[i], b, name, wave_feature, dWave))
   
    # Linearly interpolate and solve
    logNArray_interp = interp1d(logWarray, logNarray, kind='cubic')
   
    return float(logNArray_interp(np.log10(W)))

