# This is Sean Johnson's script (with some extra comments added by P. Mitchell) for doing survival analysis stats
# This allows you to compute correlation sign/strength (tau) and significance (p) for a set of points, accounting for y-value upper limits
#
# The way this works is that you count all pairs where one point is higher in both x/y compared to the second point (implies positive correlation)
# And then all pairs where one point is higher in x, but lower in y than the other point (implies anti-correlation)
# Upper limits are included in this. If you know that point is for sure lower than point 2 thanks to its upper limit, you can include it in the pos/anti correlation counts
#
# So for example (see example at bottom also), if you have a positive underlying correlation, but shallow y-detection range
# You will detect a few points in the upper right range. The fact that you have many x values on the left side, but only upper limits
# for those points - tells you that you almost certainly have a positive correlation
#
# I'm not 100% without looking it up, but it looks there is an assumption caked in that y is gaussian distributed at fixed x
# This only affects the significance of the reported correlations though, not the sign of tau (pos/neg correlation)

import numpy as np
from scipy.stats import norm

# Go from a pdf to a cdf
def pdf_to_cdf(x, pdf):
   cumulative = np.cumsum(pdf)
   cumulative = cumulative - cumulative[0]
   cdf = cumulative/cumulative[-1]
   
   return cdf

# sample some arbitary pdf super slow.
def sample_pdf(x, pdf, nSample):
   
   cdf = pdf_to_cdf(x, pdf)
   sample = np.random.random(nSample)
   
   for i in range(len(sample)):

      index = np.max(np.where(cdf < sample[i])[0])
      sample[i] = x[index+1]
   
   return sample
   
   
# kendall h test for correlation in the presence of censored data
def kendall_h(x, y, detections_y):
   
   
	# Create a ndarray to store the observations
   observations = np.zeros(len(x),
                             dtype={'names':('x', 'y', 'detection_y'), 
                                    'formats':(float, float, int)})
   observations['x'] = x
   observations['y'] = y
   observations['detection_y'] = detections_y
   
   pairs = np.zeros((len(x), len(x)), dtype={'names':('x1', 'y1', 'detection_y1',
                                                      'x2', 'y2', 'detection_y2',
                                                      'concordant', 'discordant',
                                                      'tie_x', 'tie_y', 'inuse'), 
                                    'formats':(float, float, int, float, float, \
                                               int, float, float, float, float,
                                               int)})
   
   
   # For loops are stupid but who cares
   for i in np.arange(len(x)):

      # The magic here is the "i+1" - this means each pair only appears once in the array
      # Note that pairs here does not include matching an element with itself
      for j in np.arange(i+1, len(x)):
         
         pairs[i, j]['x1'] = observations[i]['x']
         pairs[i, j]['y1'] = observations[i]['y']
         pairs[i, j]['detection_y1'] = observations[i]['detection_y']
         
         pairs[i, j]['x2'] = observations[j]['x']
         pairs[i, j]['y2'] = observations[j]['y']
         pairs[i, j]['detection_y2'] = observations[j]['detection_y']
         
         
         pairs[i, j]['inuse'] = 1
   
   # Reduce to 1d array of all possible pairs (excluding self-matches)
   pairs = pairs[pairs['inuse'] == 1]
      
   # Find pairs that are (or could be) equal to each other in x/y, given detection limits
   pairs['tie_x'] = (pairs['x1'] == pairs['x2'])*1.0 # Note *1.0 is a trick to turn boolean into float
   pairs['tie_y'] = ((pairs['y1'] == pairs['y2']) \
                     | ((pairs['y1'] < pairs['y2']) & (pairs['detection_y2'] == 0)) \
                     | ((pairs['y2'] < pairs['y1']) & (pairs['detection_y1'] == 0)))*1.0

   # Pairs where one of the points is higher in both x and y than the other point
   pairs['concordant'] = ((((pairs['x1'] > pairs['x2']) & (pairs['y1'] > pairs['y2'])) \
                           | ((pairs['x1'] < pairs['x2']) & (pairs['y1'] < pairs['y2']))) \
                           & (pairs['tie_x'] == 0) & (pairs['tie_y'] == 0))*1.0
   # Pairs where one of the points is higher in x, but the other is higher in y
   pairs['discordant'] = ((((pairs['x1'] > pairs['x2']) & (pairs['y1'] < pairs['y2'])) \
                           | ((pairs['x1'] < pairs['x2']) & (pairs['y1'] > pairs['y2']))) \
                           & (pairs['tie_x'] == 0) & (pairs['tie_y'] == 0))*1.0
   
   
   n = len(x)*1.0
   nPairs = len(pairs) - 1 # Why is there a -1 here?
   nConcordant = np.sum(pairs['concordant'])*1.0
   nDiscordant = np.sum(pairs['discordant'])*1.0
   nTieX = np.sum(pairs['tie_x'])*1.0
   nTieY = np.sum(pairs['tie_y'])*1.0
   
   nTie = np.sum((pairs['tie_x'] == 1) | (pairs['tie_y'] == 1))*1.0
   nEff = (nPairs - nTie)/nPairs*n
   # I.e. fraction of pairs we can use multiplied by number of points - gives effective number of useful data points
 
   term1 = n*(n-1.0)/2.0 - nTieX
   term2 = n*(n-1.0)/2.0 - nTieY
   
   # Another measure of the number of useful pairs
   denom = np.sqrt(term1*term2)
   
   # So positive value if points are positively correlated, negative value otherwise
   tau = (nConcordant - nDiscordant)/denom

   # Looks like a counting statistics-related error bar of some kind, using just the effective number of useful data points
   stddev = np.sqrt(2.0*(2.0*nEff+5.0))/3.0/np.sqrt(nEff*(nEff-1.0))
   
   nSigma = tau/stddev

   # .sf stands for survival function (1 - cumulative distribution function)
   # norm.sf(x) gives the area under a gaussian curve (mean 0, 1 sigma width 1) in the range x'>x, where x is the input value
   # p is twice this value, exploiting symmetry of Gaussian to give you the area under the curve with |x'| > x
   # So the higher the detection significance, the smaller the p value
   p = norm.sf(abs(nSigma))*2
   
   return tau, p

if __name__ == "__main__":

   # Test to recover a linear correlation, given detection limit ydet_max
   npoints = 100
   xmin = 0.0; xmax = 10.0
   slope = 1.0
   intercept = 0.0
   stdv = 1.0
   ydet_max = 8.0

   x = np.random.random_sample(npoints) * (xmax-xmin) + xmin
   y = x * slope + intercept + np.random.randn(npoints)* stdv

   yobs = np.copy(y)
   
   det = yobs >= ydet_max
   
   yobs[yobs < ydet_max] = ydet_max

   det_y = np.ones_like(y)
   det_y[yobs < ydet_max] = 0

   tau1, p1 = kendall_h(x, y, np.ones_like(y))
   print "Correlation slope, p value with no detection limit", tau1, p1

   tau2, p2 = kendall_h(x, yobs, det_y)
   print "Correlation slope, p value with detection limit applied", tau2, p2

   tau3, p3 = kendall_h(x[det], y[det], det_y[det])
   print "Correlation slope, p value if non-detected points are ignored", tau3, p3

   import matplotlib.pyplot as py

   py.scatter(x,y,c="k",s=5,edgecolors="none")
   py.scatter(x,yobs,c="b",s=5,edgecolors="none")
   py.show()
