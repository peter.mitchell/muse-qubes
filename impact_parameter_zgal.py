# Aims to reproduce Figure 2 from Lorrie's original draft
# Note the emitter catalogue contains impact parameter values, so this script does not need absorber info
import numpy as np
import h5py
import sys
import astropy.io.fits as fits
sys.path.append("Utilities")
import match_searchsorted as ms
from astropy.cosmology import FlatLambdaCDM

cosmo = FlatLambdaCDM(H0=70, Om0=0.3)

# Read MUSE-QuBES emitter catalogue data
path = '/home/mitchell/MUSE/Catalogs/'
catalog_version = 1.5
emitter_file = path + 'lowz_sources_v%s.fits' % str(catalog_version)

File_em = fits.open(emitter_file)

qso = File_em[1].data["QSO"]
z_qso = File_em[1].data["Z_QSO"]
z_platefit = File_em[1].data["Z_PLATEFIT"]
QOP_Marz = File_em[1].data["QOP_marz"]
impact = File_em[1].data["b_kpc"]
ID_gal = File_em[1].data["ID"]

# 0.1 comes from the min_z parameter in params.py
# 0.71 comes from the max_z parameter in params.py
ok = (z_platefit > 0.1) & (z_platefit < 0.71) & (z_platefit < z_qso) & (QOP_Marz >= 1)

'''problem = np.argmin( abs(impact-39) * abs(z_platefit-0.145)) 
print impact[problem], z_platefit[problem], ok[problem]
print z_platefit[problem], z_qso[problem], QOP_Marz[problem]
print ID_gal[problem], qso[problem]'''

####### Select also on OVI coverage ##############
# Read data on COS spectral coverage (exclude emitters that fall outside coverage)
# Note Lorrie's code is not well documented in this part - so not clear what the "gaps" correspond to generally
cos_limits_path = path + 'cos_spectra_wavelength_limits.txt'

names =   ['QSO',          'wave_init', 'wave_end']
formats = [('string', 14), 'int', 'int']
dtype = np.dtype({'names':names, 'formats':formats})

cat = np.loadtxt(cos_limits_path,skiprows=1, dtype=dtype,unpack=True)
qso_cos = cat[0]
wave_init = cat[1]
wave_end = cat[2]

wvl_OVI_1032 = 1031.926 # Wavelength of OVI line

ovi_z_init = wave_init/wvl_OVI_1032 -1
ovi_z_end = wave_end/wvl_OVI_1032 -1

always_skip_init = 1212/wvl_OVI_1032-1

qso_index = np.unique(qso, return_index=True)[1]
qso_index = np.append(qso_index, np.array(len(impact)))

ovi_z_init_em = np.zeros_like(impact)
ovi_z_end_em = np.zeros_like(impact)
for i_qso in range(len(qso_cos)):
    ovi_z_init_em[qso_index[i_qso]:qso_index[i_qso+1]] = ovi_z_init[i_qso]
    ovi_z_end_em[qso_index[i_qso]:qso_index[i_qso+1]] = ovi_z_end[i_qso]

ok = ok & (z_platefit > ovi_z_init_em) & (z_platefit < ovi_z_end_em)

gap1 = (z_platefit > always_skip_init) & (z_platefit < 1220/wvl_OVI_1032-1)
gap2 = (z_platefit > 1300/wvl_OVI_1032-1) & (z_platefit < 1307/wvl_OVI_1032-1)

prob = (qso == "PKS0552-640") & (ID_gal == 20736)

for i in range(len(qso_cos)):
    if qso_cos[i] == 'PKS0552-640':
        gap2_i = ((z_platefit > 1274/wvl_OVI_1032-1)&(z_platefit < 1291/wvl_OVI_1032-1)) | \
            ((z_platefit > 1558/wvl_OVI_1032-1)&(z_platefit < 1579/wvl_OVI_1032-1))
        gap2[qso_index[i]:qso_index[i+1]] = gap2_i[qso_index[i]:qso_index[i+1]]
    if qso_cos[i] == 'Q1354+048':
        gap1_i = (z_platefit > 0) & (z_platefit < 1220/wvl_OVI_1032-1)
        gap1[qso_index[i]:qso_index[i+1]] = gap1_i[qso_index[i]:qso_index[i+1]]
    if qso_cos[i] == 'TEX0206-048':
        gap1_i = (z_platefit > 0) & (z_platefit < 1280/wvl_OVI_1032-1)
        gap1[qso_index[i]:qso_index[i+1]] = gap1_i[qso_index[i]:qso_index[i+1]]

# Then we read in a further catalogue of emitters that are excluded
# because they reside within gaps of the COS spectral coverage

gap_gals_path = path + 'gap_galaxies.txt'

names =   ['QSO',          'ID']
formats = [('string', 14), 'int']
dtype = np.dtype({'names':names, 'formats':formats})

cat_gg = np.loadtxt(gap_gals_path,skiprows=2, dtype=dtype,unpack=True)

gg_qso = cat_gg[0]
gg_id = cat_gg[1]

for i in range(len(gg_qso)):
    exc = (gg_qso[i] == qso) & (gg_id[i] == ID_gal)

    ok = ok & (exc==False)
   
###### Read in stellar mass catalogue ########
# Exclude galaxies with zero stellar mass in catalogue
mass_file_path = path+'gal_prop_v%s.hdf5' % str(catalog_version)
mass_file = h5py.File(mass_file_path,"r")
gal_prop = mass_file["GalaxyProperties"]


QSO_gp = gal_prop["QSO"][:]
ID_gp = gal_prop["ID"][:]
logmstar_gp = gal_prop["logM"][:]
Rvir_gp = gal_prop["Rvir"][:]


QSO_unique = np.unique(QSO_gp)

logmstar = np.zeros(len(impact))
Rvir = np.zeros(len(impact))
for QSO_i in QSO_unique:
    ok_gp = QSO_gp == QSO_i
    ok_gal = qso == QSO_i
    
    ptr = ms.match(ID_gal[ok_gal], ID_gp[ok_gp])
    ok_match = ptr >= 0

    temp = logmstar[ok_gal]
    temp[ok_match] = logmstar_gp[ok_gp][ptr][ok_match]
    logmstar[ok_gal] = temp

    temp = Rvir[ok_gal]
    temp[ok_match] = Rvir_gp[ok_gp][ptr][ok_match]
    Rvir[ok_gal] = temp
    
ok = ok & (logmstar > 0)

########## Exclude galaxies within 5000 kms of QSO ###########

delta_z=5000*(1+z_qso)/299792.458
ok = ok & (z_platefit < z_qso -delta_z)


# 
'''problem = np.argmin( abs(impact[ok]-58) * abs(z_platefit[ok]-0.265))
print impact[ok][problem], z_platefit[ok][problem]
print "QSO", qso[ok][problem]
print "ID", ID_gal[ok][problem]
print "logmstar", logmstar[ok][problem]
quit()'''

print "Number of emitters that make the final selection", len(ok[ok])

#### Plotting #######
sys.path.append("Utilities")
from utilities_plotting import *
import matplotlib.cm as cm

nrow = 2; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow*1.15],
                    'legend.fontsize':9,
                    'font.size':11,
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'axes.labelsize':11})

#py.figure()

fig, axes = py.subplots(nrow,ncol)
for n,ax in enumerate(axes.flat):
    py.axes(ax)

    if n == 0:

        im = py.scatter(z_platefit[ok], impact[ok], c=logmstar[ok], cmap = cm.CMRmap_r, edgecolor="k",s=20, vmin=5.5, vmax=11.)

        for tick in ax.xaxis.get_major_ticks():
            tick.label1On = False

        py.ylim([0,300])
        py.ylabel(r"$\rho \, / \mathrm{pkpc}$")
        py.xlim([0,0.75])

        py.axvline(0.1, linestyle='--', color='grey')
        py.axvline(0.71, linestyle='--', color='grey')

        fovx = np.arange(0.1, 0.72, 0.01)
        fovy = [35*cosmo.kpc_proper_per_arcmin(i).value/60 for i in fovx]
        py.plot(fovx, fovy, color='grey')
        
        # Note this needs to go at the end, don't put anything at the bottom below the n==1 stuff either
        cbar_label = r'$\log_{10}(M_\star \, / \mathrm{M_\odot})$'
        fig.subplots_adjust(top=0.86)
        cbar_ax = fig.add_axes([0.15, 0.95, 0.77, 0.03])

        cbar = fig.colorbar(mappable=im, cax=cbar_ax, label=cbar_label,orientation = "horizontal")
        cbar.ax.tick_params(labelsize=8)


        
        #py.colorbar(im, cax=cax, orientation="horizontal")
        
    else:
        py.scatter(z_platefit[ok], impact[ok]/Rvir[ok], c=logmstar[ok], cmap = cm.CMRmap_r, edgecolor="k", vmin=5.5, vmax=11.5,s=20 )
        py.ylim([0,6.5])
        py.xlim([0,0.75])
        
        py.axvline(0.1, linestyle='--', color='grey')
        py.axvline(0.71, linestyle='--', color='grey')

        py.xlabel(r"$z_{\mathrm{gal}}$")
        py.ylabel(r"$\rho \, / R_{\mathrm{vir}}$")
        
figname = "fig2_b_zgal.pdf"
py.savefig("../Figures/"+figname)
py.show()
