# Reproduces Figure 7 from Lorrie's original draft
# Correlation stats are printed to terminal - I didn't yet bother to put them in the plot
import numpy as np
import h5py
import sys
import astropy.io.fits as fits
sys.path.append("Utilities")
import match_searchsorted as ms
from astropy.cosmology import FlatLambdaCDM
from scipy.signal import medfilt
import absorption
import os
import kendall_h_statistics as kh

cosmo = FlatLambdaCDM(H0=70, Om0=0.3)

# Read MUSE-QuBES emitter catalogue data
path = '/home/mitchell/MUSE/Catalogs/'
catalog_version = 1.5
emitter_file = path + 'lowz_sources_v%s.fits' % str(catalog_version)
spectra_location = '/home/mitchell/MUSE/COS_spectra/spectra_sowgat/'

File_em = fits.open(emitter_file)

qso = File_em[1].data["QSO"]
z_qso = File_em[1].data["Z_QSO"]
z_platefit = File_em[1].data["Z_PLATEFIT"]
QOP_Marz = File_em[1].data["QOP_marz"]
impact = File_em[1].data["b_kpc"]
ID_gal = File_em[1].data["ID"]

# 0.1 comes from the min_z parameter in params.py
# 0.71 comes from the max_z parameter in params.py 
# r.e. this value - from draft text, above this redshift you don't have coverage of both OVI components
ok = (z_platefit > 0.1) & (z_platefit < 0.71) & (z_platefit < z_qso) & (QOP_Marz >= 1)

qso_check = "HE0435-5304"
check = (qso == qso_check)
check2 = check & ok

########## Exclude absorbers within 5000 kms of QSO ###########
delta_z=5000*(1+z_qso)/299792.458
ok = ok & (z_platefit < z_qso -delta_z)

####### Select also on OVI coverage ##############
# Read data on COS spectral coverage (exclude emitters that fall outside coverage)
# Note Lorrie's code is not well documented in this part - so not clear what the "gaps" correspond to generally
cos_limits_path = path + 'cos_spectra_wavelength_limits.txt'

names =   ['QSO',          'wave_init', 'wave_end']
formats = [('string', 14), 'int', 'int']
dtype = np.dtype({'names':names, 'formats':formats})

cat = np.loadtxt(cos_limits_path,skiprows=1, dtype=dtype,unpack=True)
qso_cos = cat[0]
wave_init = cat[1]
wave_end = cat[2]

wvl_OVI_1032 = 1031.926 # Wavelength of OVI line

ovi_z_init = wave_init/wvl_OVI_1032 -1
ovi_z_end = wave_end/wvl_OVI_1032 -1

always_skip_init = 1212/wvl_OVI_1032-1

qso_index = np.unique(qso, return_index=True)[1]
qso_index = np.append(qso_index, np.array(len(impact)))

ovi_z_init_em = np.zeros_like(impact)
ovi_z_end_em = np.zeros_like(impact)
for i_qso in range(len(qso_cos)):
    ovi_z_init_em[qso_index[i_qso]:qso_index[i_qso+1]] = ovi_z_init[i_qso]
    ovi_z_end_em[qso_index[i_qso]:qso_index[i_qso+1]] = ovi_z_end[i_qso]

ok = ok & (z_platefit > ovi_z_init_em) & (z_platefit < ovi_z_end_em)

gap1 = (z_platefit > always_skip_init) & (z_platefit < 1220/wvl_OVI_1032-1)
gap2 = (z_platefit > 1300/wvl_OVI_1032-1) & (z_platefit < 1307/wvl_OVI_1032-1)

prob = (qso == "PKS0552-640") & (ID_gal == 20736)

for i in range(len(qso_cos)):
    if qso_cos[i] == 'PKS0552-640':
        gap2_i = ((z_platefit > 1274/wvl_OVI_1032-1)&(z_platefit < 1291/wvl_OVI_1032-1)) | \
            ((z_platefit > 1558/wvl_OVI_1032-1)&(z_platefit < 1579/wvl_OVI_1032-1))
        gap2[qso_index[i]:qso_index[i+1]] = gap2_i[qso_index[i]:qso_index[i+1]]
    if qso_cos[i] == 'Q1354+048':
        gap1_i = (z_platefit > 0) & (z_platefit < 1220/wvl_OVI_1032-1)
        gap1[qso_index[i]:qso_index[i+1]] = gap1_i[qso_index[i]:qso_index[i+1]]
    if qso_cos[i] == 'TEX0206-048':
        gap1_i = (z_platefit > 0) & (z_platefit < 1280/wvl_OVI_1032-1)
        gap1[qso_index[i]:qso_index[i+1]] = gap1_i[qso_index[i]:qso_index[i+1]]

# Then we read in a further catalogue of emitters that are excluded
# because they reside within gaps of the COS spectral coverage

gap_gals_path = path + 'gap_galaxies.txt'

names =   ['QSO',          'ID']
formats = [('string', 14), 'int']
dtype = np.dtype({'names':names, 'formats':formats})

cat_gg = np.loadtxt(gap_gals_path,skiprows=2, dtype=dtype,unpack=True)

gg_qso = cat_gg[0]
gg_id = cat_gg[1]

for i in range(len(gg_qso)):
    exc = (gg_qso[i] == qso) & (gg_id[i] == ID_gal)

    ok = ok & (exc==False)
    
###### Read in stellar mass catalogue ########
# Exclude galaxies with zero stellar mass in catalogue
mass_file_path = path+'gal_prop_v%s.hdf5' % str(catalog_version)
mass_file = h5py.File(mass_file_path,"r")
gal_prop = mass_file["GalaxyProperties"]


QSO_gp = gal_prop["QSO"][:]
ID_gp = gal_prop["ID"][:]
logmstar_gp = gal_prop["logM"][:]
Rvir_gp = gal_prop["Rvir"][:]


QSO_unique = np.unique(QSO_gp)

logmstar = np.zeros(len(impact))
Rvir = np.zeros(len(impact))
for QSO_i in QSO_unique:
    ok_gp = QSO_gp == QSO_i
    ok_gal = qso == QSO_i
    
    ptr = ms.match(ID_gal[ok_gal], ID_gp[ok_gp])
    ok_match = ptr >= 0

    temp = logmstar[ok_gal]
    temp[ok_match] = logmstar_gp[ok_gp][ptr][ok_match]
    logmstar[ok_gal] = temp

    temp = Rvir[ok_gal]
    temp[ok_match] = Rvir_gp[ok_gp][ptr][ok_match]
    Rvir[ok_gal] = temp

ok = ok & (logmstar > 0)

print "Number of emitters that make the final selection", len(ok[ok])

qso = qso[ok]
z_platefit = z_platefit[ok]
z_qso = z_qso[ok]
impact = impact[ok]
logmstar = logmstar[ok]
ID_gal = ID_gal[ok]

N_em = len(logmstar)

########## Finally, we read the absorber catalogue, and cross-match #########

path = '/home/mitchell/MUSE/Catalogs/'
absorber_file = path+'matched-OVI-cat-Lorrie_Sowgat_final_measurements.dat'

names =   ['QSO',          'z_abs', 'logN', 'logN_err', 'final_flag']
formats = [('string', 14), 'float', 'float', 'float', 'int']
dtype = np.dtype({'names':names, 'formats':formats})

cat = np.loadtxt(absorber_file,skiprows=1, dtype=dtype,usecols=(0,1,2,3,11), unpack=True)

qso_abs = cat[0]
z_abs = cat[1]
logN_abs = cat[2]
logN_err_abs = cat[3]
final_flag_abs = cat[4]

ok_abs = final_flag_abs >= 1
print "Total number of absorbers with final flag = 1,2,3 is", len(final_flag_abs[ok_abs])



qso_abs = qso_abs[ok_abs]
z_abs = z_abs[ok_abs]
logN_abs = logN_abs[ok_abs]
logN_err_abs = logN_err_abs[ok_abs]
final_flag_abs = final_flag_abs[ok_abs]

# Compute velocity window for each emitter
v_window = 600. # kms - search for absorbers with +/- v_window/2 around the emitter - meaning +/- 300 kms for v_window = 600 kms

logN = np.zeros_like(impact)
#logN_err = np.zeros_like(impact)

for n_e in range(N_em):
    c = 299792458 *1e-3 # kms
    delta_z=(v_window/2.) /c *(1+z_platefit[n_e]) # Dimensionless

    ok = (z_platefit[n_e] -delta_z < z_abs) & (z_abs < z_platefit[n_e] + delta_z) & (qso_abs == qso[n_e])
    
    logN[n_e] += np.log10( np.sum( np.power(10,logN_abs[ok]) ) )
    # I'm not sure about the below line - I suspect Lorrie's script is doing something different - check later
    #logN_err[n_e] += np.log10( 0.434 * np.sqrt( np.sum(logN_err_abs[ok]**2) ) / np.sum(logN_abs[ok]) )

    
'''check = (abs(0.20661651765536518-z_platefit)<0.00001) & (qso == "RXSJ02282-4057")
print z_platefit[check], ok[check]
print ID_gal[check]
print z_qso[check], QOP_Marz[check]
quit()'''

    
print "Number of emitters with a positive OVI detection", len(logN[logN>0]),"of",len(logN)

'''qso_check = "TEX0206-048"
ok = qso == qso_check


ok2 = qso_abs == qso_check
print ""
print "z abs"
print z_abs[ok2]

print "z gal, log N"
for n in range(len(z_platefit)):
    if ok[n]:
        print z_platefit[n], logN[n]
quit()'''

######## Compute upper limits for emitters without an OVI detection ##############
# Note this part is based on musequbes_getlimits.py

dv = 200.0 # Velocity interval to calculate the limit over (km/s, full width)
# Note from email exchange, Lorrie confirms this is what she used in the draft
# This is smaller than the +/- 300 kms interval used to connect absorbers with galaxies earlier on
# Presumably the value was chosen to make the upper limits consistent with the detections I suppose

nsigma = 3.0 # Significance level used for upper limit

# Read QSO spectra (and filter if not already done so)
spectrum_list = []
for QSO_i in QSO_unique:
    filename = spectra_location+'spec_'+QSO_i+'_LA.fits'

    spec_i = fits.getdata(filename)

    # Smooth the spectrum according to the median value within the closest +/- 500 pixels
    # This gives an estimate of the continuum I'm 99% sure
    # This is somewhat slow to compute, so use pretabulated version if possible
    filename_filtered = spectra_location+'spec_'+QSO_i+'_LA_median_filtered.hdf5'
   
    if os.path.isfile(filename_filtered):
        file_filtered = h5py.File(filename_filtered, "r")
        flux_filtered = file_filtered["flux"][:]
        error_filtered = file_filtered["error"][:]
        file_filtered.close()
        spec_i["FLUX"] = flux_filtered
        spec_i["ERROR"] = error_filtered
    else:
        print "Filtering QSO spectrum for QSO", QSO_i
        spec_i['FLUX'] = medfilt(spec_i['FLUX'], 501)
        spec_i['ERROR'] = medfilt(spec_i['ERROR'], 501)

        file_filtered =	h5py.File(filename_filtered, "a")
        file_filtered.create_dataset("flux", data=np.array(spec_i["FLUX"]))
        file_filtered.create_dataset("error", data=np.array(spec_i["ERROR"]))
        file_filtered.close()
        
    spectrum_list.append(spec_i)

# Compute upper limits for each emitter
logNlim = np.zeros_like(z_platefit)
for n in range(N_em):
    i_qso_u = np.where(QSO_unique == qso[n])[0][0]
    #print "Processing emitter",n,"for QSO",QSO_unique[i_qso_u]

    spec_n = spectrum_list[i_qso_u]
    feature = "OVI" # Ion
    wave_feature = 1037.0 #Wavelength of the feature (Ang) 
    Wr, WrErr = absorption.measureWr(spec_n['WAVELENGTH'], spec_n['FLUX']/spec_n['FLUX'],
                                     spec_n['ERROR']/spec_n['FLUX'],
                                     z_platefit[n], dv, feature, wave_feature)
    Wlim = WrErr*nsigma
    logNlim[n] = np.log10(absorption.calcn_linear(Wlim, feature, wave_feature))

detection = logN > -99

########## Read in cos-haloes / Johnson 15 data ##########

cos_halos_catalog = path+'COS-Halos_galaxies.tsv'

# Note the original Werk+ catalogue has upper limits computed at 2 sigma (according to Lorrie comments in her scripts)
# This catalogue being read here contains also 3 sigma upper limits computed by Lorrie herself
# I'm not sure if I have the code (or data) to reproduce this

def conv(input):
    if input == '""':
        output = np.nan
    else:
        output = float(input)
    return output

zgal_ch, logN_ch, logN_ch_3sig, R_ch = np.loadtxt(cos_halos_catalog,unpack=True,usecols=(3, 12, 15, 8), converters={12:conv,15:conv, 8:conv})

# Lorrie tweaks catalogue only to exclude one absorber with a Nan for radius
# A second tweak (which only affects kendall stats, not plot) is she removes absorbers with non detection and with no limit estimated
ok_ch = (np.isnan(R_ch)==False) & ( (np.isnan(logN_ch)==False) | (np.isnan(logN_ch_3sig)==False))
zgal_ch = zgal_ch[ok_ch]; logN_ch = logN_ch[ok_ch]; logN_ch_3sig = logN_ch_3sig[ok_ch]; R_ch = R_ch[ok_ch]

det_ch = np.isnan(logN_ch)==False
print "Cos-haloes sources, total, detected, non-detected", len(zgal_ch), len(zgal_ch[det_ch]), len(zgal_ch[det_ch==False])

johnson_catalog = path+'johnson15_OVI.fits'
hdu_j15 = fits.open(johnson_catalog)
logNOVI_j15 = hdu_j15[1].data['logNHOVI']
logNOVI_3sig_j15 = hdu_j15[1].data['logNOVI_3sig_lim']
limit_j15 = hdu_j15[1].data['l_logNHOVI']
zgal_j15 = hdu_j15[1].data['zgal']

logNOVI_j15[limit_j15=='<'] = np.nan

d_Rvir_j15 = hdu_j15[1].data["d_Rh"]
# Lorrie does one tweak to this catalogue which is to remove 3 absorbers with impact parameter > 8 R_vir
# She also excludes absorbers with no detection and no upper limit (only relevant for kendle stats)
ok_j15 = (d_Rvir_j15 <= 8.0) & ( (np.isnan(logNOVI_j15)==False) | (np.isnan(logNOVI_3sig_j15)==False))
logNOVI_j15 = logNOVI_j15[ok_j15]
zgal_j15 = zgal_j15[ok_j15]
limit_j15 = limit_j15[ok_j15]
logNOVI_3sig_j15 = logNOVI_3sig_j15[ok_j15]

# Extra data I'm not using in this script
#impact = hdu[1].data['d']
#qso = hdu[1].data['Name']
#error = hdu[1].data['e_logNHOVI']
#mass = hdu[1].data['logM_']
#survey = hdu_j15[1].data['Survey']

det_j15 = np.isnan(logNOVI_j15)==False

print "Johnson+15 catalogue, total, detected, non-detected", len(zgal_j15), len(zgal_j15[det_j15]), len(zgal_j15[det_j15==False])

########## Compute correlation statistics ##############

# For just Muse-QuBES by itself
logN_wlim = np.copy(logN)
logN_wlim[detection==False] = logNlim[detection==False]
tau, p = kh.kendall_h(z_platefit, logN_wlim, detection)
print "Correlation statistics for Muse-Qubes only"
print "N_det, N_ndet, tau, p", len(detection[detection]), len(detection[detection==False]), tau, p

# Combination of Muse-qubes, cos-haloes, and johnson+15
logN_tot = np.concatenate((logN, logNOVI_j15, logN_ch))
logN_wlim = np.copy(logN_tot)
detection_tot = np.concatenate(( detection, det_j15, det_ch))
logNlim_tot = np.concatenate(( logNlim, logNOVI_3sig_j15, logN_ch_3sig ))
logN_wlim[detection_tot==False] = logNlim_tot[detection_tot==False]
z_gal_tot = np.concatenate(( z_platefit, zgal_j15, zgal_ch ))
tau, p = kh.kendall_h(z_gal_tot, logN_wlim, detection_tot)
print "Correlation statistics for Muse-Qubes + Johnson15+ + cos-haloes"
print "N_det, N_ndet, tau, p", len(detection_tot[detection_tot]), len(detection_tot[detection_tot==False]), tau, p

#### Plotting #######
sys.path.append("Utilities")
from utilities_plotting import *
import matplotlib.cm as cm

nrow = 1; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'legend.fontsize':9,
                    'font.size':11,
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.97,
                    'axes.labelsize':11})

py.figure()

subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    # Muse-Qubes
    py.scatter(z_platefit[detection==False], logNlim[detection==False], c="w", edgecolor="r",s=20)
    py.scatter(z_platefit, logN, c="r", edgecolor="k",s=20)

    # Cos-haloes
    py.scatter(zgal_ch, logN_ch, c="k", edgecolor="k", s=10, marker="s")
    py.scatter(zgal_ch, logN_ch_3sig, c="w", edgecolor="k", s=10, marker="s")

    # Johnson+15
    py.scatter(zgal_j15, logNOVI_j15, c="k", edgecolor="k", s=10, marker="*")
    py.scatter(zgal_j15, logNOVI_3sig_j15, c="w", edgecolor="k", s=10, marker="*")
    
    #py.ylim([0,300])
    py.ylabel(r"$\log_{10}(N_{\mathrm{OVI}} \, / \mathrm{cm^{-2}})$")
    #py.xlim([0,0.75])
    py.xlabel(r"$z_{\mathrm{gal}}$")
               
figname = "fig7_NOVI_zgal.pdf"
py.savefig("../Figures/"+figname)
py.show()
